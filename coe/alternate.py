def alternate(s):
    distinct_chars1 = set(s)
    distinct_chars = []
    for char in distinct_chars1:
        distinct_chars.append(char)
    longest = {}
    for char1 in distinct_chars:
        for char2 in distinct_chars:
            if char1 == char2:
                continue
            last_char = ''
            for char in s:
                if char == char1 or char == char2:
                    if last_char == '' or char not in last_char[-1]:
                        last_char += char
                    else:
                        last_char = char 
                        break
            if last_char != "" and len(last_char) != 1:
                longest[last_char] = len(last_char)
        
    if len(longest) == 0:
        return 0
    return max(longest.values())
# print(alternate('beabeefeab'))
# print(alternate("asdcbsdcagfsdbgdfanfghbsfdab"))
