def catsandmouse(x,y,z):
    difx = abs(x - z)
    dify = abs(y - z)
    if difx > dify: 
        return "Cat B" 
    return "Cat A" if difx < dify else "Mouse C"
# print(catsandmouse(50,50,20))
