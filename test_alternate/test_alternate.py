from coe.alternate import alternate

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_beabeefeab_should_5_(self):
        char_p = "beabeefeab"
        expected_check = 5

        check = alternate.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_asdcbsdcagfsdbgdfanfghbsfdab_should_8_(self):
        char_p = "asdcbsdcagfsdbgdfanfghbsfdab"
        expected_check = 8

        check = alternate.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_aaaaa_should_0_(self):
        char_p = "aaaaa"
        expected_check = 0

        check = alternate.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_ab_should_2_(self):
        char_p = "ab"
        expected_check = 2

        check = alternate.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_asvkugfiugsalddlasguifgukvsa_should_0_(self):
        char_p = "asvkugfiugsalddlasguifgukvsa"
        expected_check = 0

        check = alternate.alter_nate(char_p)

        self.assertEqual(expected_check, check)