from coe.alternatingCharacters import alternatingCharacters

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_AAAA_should_3_(self):
        char_p = "AAAA"
        expected_check = 3

        check = alternatingCharacters.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_BBBBB_should_4_(self):
        char_p = "BBBBB"
        expected_check = 4

        check = alternatingCharacters.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_ABABABAB_should_0_(self):
        char_p = "ABABABAB"
        expected_check = 0

        check = alternatingCharacters.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_BABABA_should_0_(self):
        char_p = "BABABA"
        expected_check = 0

        check = alternatingCharacters.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_AAABBB_should_4_(self):
        char_p = "AAABBB"
        expected_check = 4

        check = alternatingCharacters.alter_nate(char_p)

        self.assertEqual(expected_check, check)