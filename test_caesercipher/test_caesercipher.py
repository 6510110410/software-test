from coe.caesercipher import caesarCipher

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_12_Hello_World_4_should_Lipps_Asvph_(self):
        cheap = 12
        char_p = "Hello_World!"
        chapter = 4
        expected_check = "Lipps_Asvph!"

        check = caesarCipher.alter_nate(cheap,char_p,chapter)

        self.assertEqual(expected_check, check)
    def test_give_11_Ciphering_2_should_okffng_Qwvb_(self):
        cheap = 11
        char_p = "middle-Outz"
        chapter = 2
        expected_check = "okffng-Qwvb"

        check = caesarCipher.alter_nate(cheap,char_p,chapter)

        self.assertEqual(expected_check, check)
    def test_give_10_Ciphering_26_should_Ciphering_(self):
        cheap = 10
        char_p = "Ciphering."
        chapter = 26
        expected_check = "Ciphering."
        

        check = caesarCipher.alter_nate(cheap,char_p,chapter)

        self.assertEqual(expected_check, check)
    def test_give_10_www_abc_xy_87_should_fff_jkl_gh_(self):
        cheap = 10
        char_p = "www.abc.xy"
        chapter = 87
        expected_check = "fff.jkl.gh"

        check = caesarCipher.alter_nate(cheap,char_p,chapter)

        self.assertEqual(expected_check, check)
    def test_give_4_D3q4_0_should_D3q4_(self):
        cheap = 4
        char_p = "D3q4"
        chapter = 0
        expected_check = "D3q4"

        check = caesarCipher.alter_nate(cheap,char_p,chapter)

        self.assertEqual(expected_check, check)