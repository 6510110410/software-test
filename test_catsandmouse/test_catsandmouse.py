import unittest

from coe.catsandmouse import catsandmouse


class CatsAndMouse(unittest.TestCase):
    def test_give_1_2_3_should_cat_b(self):
        cat_a = 1
        cat_b = 2
        mouse_c = 3
        expected_result = "Cat B"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_55_should_cat_b(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 55
        expected_result = "Cat B"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_30_should_cat_a(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 30
        expected_result = "Cat A"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_0_100_50_should_mouse_c(self):
        cat_a = 0
        cat_b = 100
        mouse_c = 50
        expected_result = "Mouse C"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_35_40_30_should_cat_a(self):
        cat_a = 35
        cat_b = 40
        mouse_c = 30
        expected_result = "Cat A"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
    
    def test_give_100_20_60_should_mouse_c(self):
        cat_a = 100
        cat_b = 20
        mouse_c = 60
        expected_result = "Mouse C"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_75_70_60_should_mouse_c(self):
        cat_a = 120
        cat_b = 55
        mouse_c = 70
        expected_result = "Cat B"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_35_40_30_should_cat_a(self):
        cat_a = 75
        cat_b = 90
        mouse_c = 65
        expected_result = "Cat A"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
    
    def test_give_100_20_60_should_mouse_c(self):
        cat_a = 150
        cat_b = 10
        mouse_c = 80
        expected_result = "Mouse C"

        result = catsandmouse.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)