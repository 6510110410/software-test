from coe.fizzbuzz import under

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_3_should_fizz_(self):
        point = 3
        expected_clusion = "Fizz"

        clusion = under.fizzbuzz(point)

        self.assertEqual(expected_clusion, clusion)
    def test_give_5_should_Buzz_(self):
        point = 5
        expected_clusion = "Buzz"

        clusion = under.fizzbuzz(point)

        self.assertEqual(expected_clusion,clusion)
    
    def test_give_15_should_FizzBuzz_(self):
        point = 15
        expected_clusion = "FizzBuzz"

        clusion = under.fizzbuzz(point)

        self.assertEqual(expected_clusion,clusion)

    def test_give_6_should_fizz_(self):
        point = 6
        expected_clusion = "fizz"

        clusion = under.fizzbuzz(point)

        self.assertEqual(expected_clusion,clusion)

    def test_give_10_should_Buzz_(self):
        point = 10
        expected_clusion = "Buzz"

        clusion = under.fizzbuzz(point)

        self.assertEqual(expected_clusion,clusion)
    

    
