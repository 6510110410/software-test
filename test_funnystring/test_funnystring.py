from coe.funnystring import funnyString

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_acxz_should_Funny_(self):
        char_p = "acxz"
        expected_check = "Funny"

        check = funnyString.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_bcxz_should_Not_Funny_(self):
        char_p = "bcxz"
        expected_check = "Not Funny"

        check = funnyString.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_ivvkxq_should_Not_Funny_(self):
        char_p = "ivvkxq"
        expected_check = "Not Funny"

        check = funnyString.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_ivvkx_should_Not_Funny_(self):
        char_p = "ivvkx"
        expected_check = "Not Funny"

        check = funnyString.alter_nate(char_p)

        self.assertEqual(expected_check, check)
    def test_give_holtm_should_Not_Funny_(self):
        char_p = "holtm"
        expected_check = "Not Funny"

        check = funnyString.alter_nate(char_p)

        self.assertEqual(expected_check, check)