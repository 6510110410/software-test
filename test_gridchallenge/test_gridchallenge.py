from coe.gridchallenge import gridChallenge

import unittest

class FizzBuzz(unittest.TestCase):
    def test_give_ebacd_fghij_olmkn_trpqs_xywuv_should_YES_(self):
        challenge = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        expected_challenge = "YES"

        challenge = gridChallenge.chal_lenge(challenge)

        self.assertEqual(expected_challenge, challenge)
    def test_give_ppp_ypp_wyw_should_NO_(self):
        challenge = ["ppp","ypp","wyw"]
        expected_challenge = "NO"

        challenge = gridChallenge.chal_lenge(challenge)

        self.assertEqual(expected_challenge, challenge)
    def test_give_tjxxx_xtxxj_rzzzz_zzrzz_rzzzz_should_YES_(self):
        challenge = ["tjxxx","xtxxj","rzzzz","zzrzz","rzzzz"]
        expected_challenge = "YES"

        challenge = gridChallenge.chal_lenge(challenge)

        self.assertEqual(expected_challenge, challenge)
    def test_give_p_should_YES_(self):
        challenge = ["p"]
        expected_challenge = "YES"

        challenge = gridChallenge.chal_lenge(challenge)

        self.assertEqual(expected_challenge, challenge)
    def test_give_rpb_hot_qra_should_NO_(self):
        challenge = ["rpb","hot","qra"]
        expected_challenge = "NO"

        challenge = gridChallenge.chal_lenge(challenge)

        self.assertEqual(expected_challenge, challenge)