from coe.number_utils import is_prime_list

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_prime(self):
        prime_list = [1, 2, 3]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_4_5_6_is_not_prime(self):
        prime_list = [4, 5, 6]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
        # self.assertTrue(is_prime)
    def test_give_7_11_13_is_prime(self):
        prime_list = [7, 11, 13]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)
    def test_give_8_9_10_is_not_prime(self):
        prime_list = [8, 9, 10]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    def test_give_17_19_23_is_prime(self):
        prime_list = [17, 19, 23]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)
    def test_give_12_14_15_is_not_prime(self):
        prime_list = [12, 14, 15]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    def test_give_16_18_20_is_not_prime(self):
        prime_list = [16, 18, 20]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)